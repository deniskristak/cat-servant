#!/usr/bin/python3.8

class abstractController:
    import cv2
    import config
    import os
    from datetime import datetime
    import time
    import smtplib
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.utils import COMMASPACE
    from email.mime.image import MIMEImage
    from email.header import Header

    # config function - takes important variables used later on, from config
    def getConfig( self ):
        self.pathToPhotos = self.config.photoParameters['pathToPhotos']
        self.photoName = self.config.photoParameters['photoName']
        self.nowString = self.config.nowString
        self.daysBeforePhotoIsRemoved = self.config.photoParameters['daysBeforePhotoIsRemoved']

    def __init__(self):
        self.getConfig()
