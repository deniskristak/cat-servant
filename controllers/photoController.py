#!/usr/bin/python3.8

from .abstractController import abstractController

class photoController(abstractController):

    # this func is responsible for taking and saving photo to a specific folder
    def takeAPhoto(self):
        # only after all photo has been successfully taken and saved, this variable changes to True
        phototaken = False

        # saves current path
        currPath = self.os.getcwd()

        # changes path to photo-target directory
        self.os.chdir(self.pathToPhotos)

        self.removeOldPhotos()

        # creates a camera class
        # 0 is a camera's number (if multiple are present, can be suspect to change)
        cam = self.cv2.VideoCapture(0)

        # reads selected camera's input
        ret, frame = cam.read()
        if not ret:
            # todo put in logfile
            asts = '**************************************************'
            print(asts + '\n' +asts+ '\n'+asts+'\n\n' + "FAILED TO GRAB FRAME" + '\n\n'+asts+'\n'+asts+'\n' + asts)

        # takes photo and saves it in a file
        phototaken = self.cv2.imwrite(self.photoName, frame)

        # returns to the original directory
        self.os.chdir(currPath)

        # destroys the class instance
        cam.release()

        # returning False or True to take proper actions
        return phototaken

    # this function deletes old photos to prevents disc overflow
    def removeOldPhotos(self):

        # list of all files in our pathToPhotos folder
        fileListpathToPhotos = self.os.listdir( self.pathToPhotos )

        # iterates through all files in target directory
        for f in fileListpathToPhotos:

            # system returns modification time of the file
            modifiedTime = self.os.path.getmtime(f)

            # if the file hasnt been touched for more than 7 days, it will be removed
            if ( self.time.time() - modifiedTime) // (24 * 3600) >= self.daysBeforePhotoIsRemoved:

                self.os.unlink(f)

                # logs (if corrected output is set) removal of old photos
                print('{} removed'.format(f))


    # funcion responsible for sending photos by email
    def sendPhotosByMail(self, send_to, subject, text, files ):

        # must be a list
        assert isinstance(send_to, list)

        # connects to localhost 'postfix' mail service with smtp
        smtp = self.smtplib.SMTP()
        # if in docker's container, use 0.0.0.0, else, use 'localhost'
        smtp.connect('0.0.0.0')

        # configures message
        msg = self.MIMEMultipart()
        msg['From'] = 'cat-servant'
        msg['To'] = self.COMMASPACE.join(send_to)
        print( "\n" + msg['To'])
        msg['Date'] = self.nowString
        msg['Subject'] = subject
        msg.attach(self.MIMEText(text))

        # iterates through files to send, reads them as binary, attaches to email
        for f in files or []:
            print( f + "\n" )
            with open(f, "rb") as fil:
                part = self.MIMEApplication(
                    fil.read(),
                    Name = self.os.path.basename(f)
                )
            part['Content-Disposition'] = 'attachment; filename="%s"' % self.os.path.basename(f)
            msg.attach(part)

        # sends the email itself. send_to must be passed as a list
        smtp.sendmail(msg['From'], send_to , msg.as_string())
