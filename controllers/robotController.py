#!/usr/bin/python3.8

from .abstractController import abstractController
import RPi.GPIO as GPIO
from time import sleep

# a class to controll robot
class robotController(abstractController):

    def configure(self):

        # motor driver setup
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.config.in1,GPIO.OUT)
        GPIO.setup(self.config.in2,GPIO.OUT)
        GPIO.setup(self.config.en,GPIO.OUT)
        GPIO.output(self.config.in1,GPIO.LOW)
        GPIO.output(self.config.in2,GPIO.LOW)
        self.p=GPIO.PWM(self.config.en,1000)


    # rotates a motor for number of seconds according to configutation for param rotationSeconds
    def rotate(self):

        # starts a motor
        self.p.start(self.config.motorSpeed)
        GPIO.output(self.config.in1,GPIO.HIGH)
        GPIO.output(self.config.in2,GPIO.LOW)

        # waits for some time
        sleep(self.config.rotationSeconds)

        # stops a motor
        GPIO.output(self.config.in1,GPIO.LOW)
        GPIO.output(self.config.in2,GPIO.LOW)
        GPIO.cleanup()
