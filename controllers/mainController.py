#!/usr/bin/python3.8

from .abstractController import abstractController
from .photoController import photoController
from .robotController import robotController

class mainController(abstractController):

    # controls robot to pour food for cats by rotating DC motor
    def pourCatFood(self):
        feeder = robotController()
        feeder.configure()
        feeder.rotate()

        pass

    # when food is poured, takes and sends a photo for a human to ensure that all went well and cats have food.
    def takeSaveUploadPhoto(self):
        # create an instance of photoController class
        photo = photoController()

        # takes a photo
        photo.takeAPhoto()

        # sets full path to file for email
        fileForMail = self.config.photoParameters['pathToPhotos'] + '/' + self.config.photoParameters['photoName']

        # sends photos by mail
        photo.sendPhotosByMail( send_to = self.config.send_to, subject = self.config.emailSubject, text = self.config.emailText, files=[ fileForMail ] )
