#!/usr/bin/python3.8

from controllers.mainController import mainController

def run():
    runner = mainController()

    runner.pourCatFood()
    runner.takeSaveUploadPhoto()

run()
