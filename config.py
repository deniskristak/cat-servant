#!/usr/bin/python3.8

from datetime import datetime

#################################################################################
###################### EMAIL CONFIGURATION START ################################
# current time string e.g. 2020-08-08 01:55:59
nowString = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
send_to = ['kristak@logio.cz', 'davepilecek@gmail.com', 'davidpilecek@seznam.cz', 'denis.kristak@gmail.com'] #, 'ninafod@gmail.com']
emailSubject = 'cat photos from '+ nowString
emailText = 'Here is the most recent photo from Cat Servant'
########################EMAIL CONFIGURATION END #################################
#################################################################################



#################################################################################
###################### PHOTO CONFIGURATION START ################################
photoParameters = {

  "pathToPhotos": "/media/cat-servant-media/photos",

  # suffix (.jpg is NECESSARY)
  "photoName": "feeder_" + nowString + '.jpg',

  # how many days does a photo have to be untouched (not modified) to get removed
  "daysBeforePhotoIsRemoved": 7
}
####################### PHOTO CONFIGURATION END ################################
################################################################################


#################################################################################
####################### MOTOR CONFIGURATION START ################################

# hardware pins configuration - DO NOT CHANGE
in1 = 15
in2 = 14
en = 18


# how many seconds will motor rotate
rotationSeconds=1

# on what speed will motor operate
motorSpeed = 75

####################### MOTOR CONFIGURATION END ################################
################################################################################
